﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace APIVendas.Models {
    public class Vendedor {
        public Vendedor() {
            Vendas = new Collection<Venda>();
        }
        public int VendedorId { get; set; }
        public string? CPF { get; set; }
        public string? Nome { get; set; }
        public string? Email { get; set;}
        public string? Telefone { get; set; }
        public ICollection<Venda>? Vendas { get; set; }
    }
}
