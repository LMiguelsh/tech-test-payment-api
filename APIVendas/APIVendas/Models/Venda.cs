﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace APIVendas.Models {
    public class Venda {
        public Venda(string? status) {
            status = "Aguardando Pagamento";
            Status = status;
        }

        public int VendaId { get; set; }
        public DateTime DtVenda { get; set; }
        [Required]
        public string? Itens { get; set; }
        public string? Status { get; set; }
        public int VendedorId { get; set; }
        public Vendedor? Vendedor { get; set; }
        

    }
}
