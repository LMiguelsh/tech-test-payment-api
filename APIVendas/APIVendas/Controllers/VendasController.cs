﻿using APIVendas.Data;
using APIVendas.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIVendas.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class VendasController : ControllerBase {
        private readonly AppDbContext _context;
        public VendasController(AppDbContext context) {
            _context = context;
        }

        [HttpGet("{id:int}", Name = "ObterVenda")]
        public ActionResult<Venda> GetVenda(int id) {

            try {
                var venda = _context.Vendas.FirstOrDefault(e => e.VendaId == id);
                if (venda is null) {
                    return NotFound("Venda não encontrada...");
                }
                return venda;
            }
            catch (Exception) {

                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Ocorreu um problema ao tratar sua solicitação.");
            }
        }

        [HttpPost]
        public ActionResult PostVenda(Venda venda) {
                if (venda is null)
                    return BadRequest();

                _context.Vendas.Add(venda);
                _context.SaveChanges();


                return new CreatedAtRouteResult("ObterVenda",
                    new { id = venda.VendaId }, venda);
        }

        [HttpPut("{id:int}", Name = "AtualizaStatus")]
        public ActionResult PutStatus(int id, Venda venda) {
            try {
                if (id != venda.VendaId) {
                    return BadRequest();
                }

                if (venda.Status == "Aguardando Pagamento") {
                    _context.Entry(venda.Status).State = EntityState.Modified;
                    if (venda.Status == "Cancelada" || venda.Status == "Pagamento Aprovado")
                        _context.SaveChanges();
                }
                else if (venda.Status == "Pagamento Aprovado") {
                    _context.Entry(venda.Status).State = EntityState.Modified;
                    if (venda.Status == "Enviado para Transportadora" || venda.Status == "Cancelada")
                        _context.SaveChanges();
                }
                else if (venda.Status == "Enviado para Transportadora") {
                    _context.Entry(venda.Status).State = EntityState.Modified;
                    if (venda.Status == "Entregue")
                        _context.SaveChanges();
                }
                else {
                    return BadRequest("Atualização não permitida.");
                }

                return Ok(venda);
            }
            catch (Exception) {

                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Ocorreu um problema ao tratar sua solicitação.");

            }
        }

        }
    }
