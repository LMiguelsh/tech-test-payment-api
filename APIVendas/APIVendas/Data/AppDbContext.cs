﻿using APIVendas.Models;
using Microsoft.EntityFrameworkCore;

namespace APIVendas.Data {
    public class AppDbContext : DbContext {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Venda>? Vendas { get; set; }
        public DbSet<Vendedor>? Vendedores { get; set; }

    }
}